/**
 *
 * @copyright	Copyright (C) 2017 maxifutsal.fr. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

function searchPlayer(val)
{
	jQuery('#filter_search').val(val);
	jQuery('#adminForm').submit();
}