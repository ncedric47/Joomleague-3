/**
 *
 * @copyright	Copyright (C) 2017 maxifutsal.fr. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

Joomla.submitbutton = function(task) {
	var res = true;
	var form = jQuery('adminForm');

	if (task == 'teamstaff.cancel') {
		Joomla.submitform(task);
		if(window.parent.SqueezeBox) {
			window.parent.SqueezeBox.close();
		}
		return;
	}
	
	if (res) {
		Joomla.submitform(task);
	} else {
		return false;
	}
}