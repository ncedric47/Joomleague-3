<?php 
/**
 *
 * @copyright	Copyright (C) 2017 maxifutsal.fr. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @link		http://www.joomleague.at
 */

defined('_JEXEC') or die; // no direct access

DEFINE('PROJECT_REGULAR',		0);
DEFINE('PROJECT_DIVISIONS',		1);
DEFINE('PROJECT_TOURNAMENT',		2);
DEFINE('PROJECT_FRIENDLY_MATCHES',	3);
