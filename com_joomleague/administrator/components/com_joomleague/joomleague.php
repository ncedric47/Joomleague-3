<?php
/**
 *
 * @copyright	Copyright (C) 2017 maxifutsal.fr. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @link		http://www.joomleague.at
 * 
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.tabstate');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_joomleague')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);


require_once JPATH_ROOT.'/components/com_joomleague/joomleague.core.php';
// Require the base controller
require_once JPATH_COMPONENT.'/controller.php';
require_once JPATH_COMPONENT.'/helpers/jlparameter.php';
require_once JLG_PATH_ADMIN.'/helpers/jltoolbarhelper.php';
require_once JLG_PATH_ADMIN.'/controllers/jlgform.php';
require_once JLG_PATH_ADMIN.'/models/jlgitem.php';
require_once JLG_PATH_ADMIN.'/models/jlglist.php';
require_once JLG_PATH_SITE.'/helpers/extensioncontroller.php';

$filter = JFilterInput::getInstance();
$input = JFactory::getApplication()->input;
$task = 'display';
$command  = $input->get('task', 'display');
if (is_array($command))
{
	$command = $filter->clean(array_pop(array_keys($command)), 'cmd');
}
else
{
	$command = $filter->clean($command, 'cmd');
}
if (strpos($command, '.') !== false)
{
	list ($type, $task) = explode('.', $command);
}
$controller	= JLGController::getInstance('joomleague');
$controller->execute($task);
$controller->redirect();
