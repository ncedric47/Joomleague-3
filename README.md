/**
 * JoomLeague 3 readme.md
 * Date 2017-01-01
 *
 * http://www.maxifutsal.fr
 *
 * /


/**-----------------------------------------------------------------------------
 * ----------------------------------INSTALLATION-------------------------------
 * -----------------------------------------------------------------------------
 * /

 1. Backup your Joomla Database
 2. Backup your Joomla Files and Folders !
 3. Install JoomLeague via Joomla Component Installer.
 4. Create your own League, Season and Project or use the JoomLeague Import for
 demonstration purposes.
 5. Your installation and the first configuration is conpleted

/**-----------------------------------------------------------------------------
 * ------------------------------------UPGRADE----------------------------------
 * -----------------------------------------------------------------------------
 * /